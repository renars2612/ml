class FestivalsController < ApplicationController

  #before_action :authenticate_user!, :is_admin?, :except => [:index, :show]

  def index
    @festivals = Festival.all
    unless params[:search].blank?
      search_string = "%#{params[:search]}%"
      @festivals = @festivals.where( "name LIKE ? OR location LIKE ?", search_string, search_string )
    end
    unless params[:ajax].blank?
      unless @festivals.blank?
        render "_list", :layout => false
      else
        render "_list_not_found", :layout => false
      end
    end
  end

  def show
    @festival = Festival.find_by_id!( params[:id] )
  end

  def edit
    @festival = Festival.find_by_id!( params[:id] )
  end

  def new
    @festival = Festival.new
  end

  def create
    @festival = Festival.new( clean_params )
    if @festival.save
      redirect_to festivals_path, :notice => "Festival was successfully created."
    else
      render :new
    end
  end

  def update
    @festival = Festival.find_by_id!( params[:id] )
    @festival.assign_attributes( clean_params )
    if @festival.save
      redirect_to festivals_path, :notice => "Festival was successfully created."
    else
      render :edit
    end
  end

  def destroy
    @festival = Festival.find_by_id!( params[:id] )

    @festival.destroy
    redirect_to news_index_path, :notice => "Festival was successfully destroyed."
  end

  private

  def clean_params
    params.require(:festival).permit(:id, :name, :bio_lv, :bio_en, :bio_ru, :start_date, :end_date, :location, :twitter, :facebook, :draugiem, :email, :webpage, :contact, :image, :phone)
  end


end
