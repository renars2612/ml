class AuthorsController < ApplicationController

  before_action :authenticate_user!, :is_admin?, :except => [:index, :show]

  def index
    @items = Author.all

    unless params[:search].blank?
      search_string = "%#{params[:search]}%"
      @items = Author.where( "name LIKE ? OR bio_lv LIKE ? OR bio_en LIKE ? OR bio_ru LIKE ?", search_string, search_string, search_string, search_string )
    end

    unless params[:ajax].blank?
      unless @items.blank?
        render "_author_list", :layout => false
      else
        render "_list_not_found", :layout => false
      end
    end
  end

  def show
    @item = Author.find_by_id!( params[:id] )
  end

  def edit
    @item = Author.find_by_id!( params[:id] )
  end

  def new
    @item = Author.new
  end

  def create
    @item = Author.new( clean_params )
    if @item.save
      redirect_to author_path( :id => @item.id ), :notice => "Author was successfully created."
    else
      render :new
    end
  end

  def update
    @item = Author.find_by_id!( params[:id] )
    @item.assign_attributes( clean_params )
    if @item.save
      redirect_to author_path( :id => @item.id ), :notice => "Author was successfully created."
    else
      render :edit
    end
  end

  def destroy
    @item = Author.find_by_id!( params[:id] )

    @item.destroy
    redirect_to authors_path, :notice => "Author was successfully destroyed."
  end

  private

  def clean_params
    params.require(:author).permit(:id, :name, :bio_lv, :bio_en, :bio_ru, :facebook, :twitter, :draugiem, :spotify, :email, :webpage, :contact, :genre_id, :image, :phone)
  end

end
