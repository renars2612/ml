class ArtistsController < ApplicationController

  before_action :authenticate_user!, :is_admin?, :except => [:index, :show]

  def index
    @artists = Artist.all

    unless params[:search].blank?
      search_string = "%#{params[:search]}%"
      @artists = @artists.where( "name LIKE ? ", search_string )
    end

    unless params[:ajax].blank?
      unless @artists.blank?
        if (params[:search].blank?)
          render "_artist_list", :layout => false
        else
          render "_artist_list_search", :layout => false
        end
      else
        render "_list_not_found", :layout => false
      end
    end
  end

  def show
    @artist = Artist.find_by_id!( params[:id] )
  end

  def edit
    @artist = Artist.find_by_id!( params[:id] )
  end

  def new
    @artist = Artist.new
  end

  def create
    @artist = Artist.new( clean_params )

    genre_ids.each do |gid|
      ArtistsGenre.create( :artist_id => @artist.id, :genre_id => gid )
    end

    if @artist.save
      redirect_to artist_path( :id => @artist.id ), :notice => "Artist was successfully created."
    else
      render :new
    end
  end

  def update
    @artist = Artist.find_by_id!( params[:id] )

    ActiveRecord::Base.connection.execute("DELETE FROM `artists_genres` WHERE `artists_genres`.`artist_id` = #{@artist.id}")
    ActiveRecord::Base.transaction do
      genre_ids.each do |gid|
        ArtistsGenre.create( :artist_id => @artist.id, :genre_id => gid )
      end
    end

    @artist.assign_attributes( clean_params )
    if @artist.save
      redirect_to artist_path( :id => @artist.id ), :notice => "Artist was successfully updated."
    else
      render :edit
    end
  end

  def destroy
    @artist = Artist.find_by_id!( params[:id] )

    @artist.destroy
    redirect_to artists_path, :notice => "Artist was successfully destroyed."
  end

  private

  def genre_ids
    params[:artist][:genre_ids].delete_if {|x| x.blank? }
  end

  def clean_params
    params.require(:artist).permit(:id, :name, :bio_lv, :bio_en, :bio_ru, :facebook, :twitter, :draugiem, :spotify, :soundcloud, :email, :webpage, :contact, :image, :spotlight, :phone, :artists_genres_attributes =>
      [
        :artist_id, :genre_id
      ])
  end

end
