class HomeController < ApplicationController
  def index
    @spotlight_artist = Artist.where("spotlight = 1").first
    if @spotlight_artist.blank?
      @spotlight_artist = Artist.offset(rand(Artist.count)).first
    end
    @posts = Post.where( "language = ?", all_languages.index(locale) ).limit(10)
  end
end
