class NewsController < ApplicationController

  before_action :authenticate_user!, :is_admin?, :except => [:index, :show]

  def index
    @categories = Category.all
    @posts = Post.where( "language = ?", all_languages.index(locale) )

    unless params[:search].blank?
      search_string = "%#{params[:search]}%"
      @posts = @posts.where( "title LIKE ? OR content LIKE ?", search_string, search_string )
    end

    unless params[:category].blank?
      @posts = @posts.where( "category_id = ?", params[:category] )
    end

    @posts = @posts.paginate(:page => params[:page])

    unless params[:ajax].blank?
      unless @posts.blank?
        render "_news_list", :layout => false
      else
        render "_list_not_found", :layout => false
      end
    end
  end

  def show
    @post = Post.find_by_id!( params[:id] )
  end

  def edit
    @post = Post.find_by_id!( params[:id] )
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new( clean_params )
    if @post.save
      redirect_to news_path( :id => @post.id ), :notice => "Post was successfully created."
    else
      render :new
    end
  end

  def update
    @post = Post.find_by_id!( params[:id] )
    @post.assign_attributes( clean_params )
    if @post.save
      redirect_to news_path( :id => @post.id ), :notice => "Post was successfully created."
    else
      render :edit
    end
  end

  def destroy
    @post = Post.find_by_id!( params[:id] )

    @post.destroy
    redirect_to news_index_path, :notice => "Post was successfully destroyed."
  end

  private

  def clean_params
    params.require(:post).permit(:id, :title, :content, :language, :publish_date, :image, :category_id )
  end
end
