class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :set_locale

  before_action :load_common_resources
  before_action :configure_permitted_parameters, if: :devise_controller?

  before_filter :redirect_subdomain

  def redirect_subdomain
    if request.host == 'www.musiclatvia.lv'
      redirect_to 'http://musiclatvia.lv' + request.fullpath, :status => 301
    end
    if request.host == 'musicex.lv'
      redirect_to 'http://musiclatvia.lv' + request.fullpath, :status => 301
    end
    if request.host == 'www.musicex.lv'
      redirect_to 'http://musiclatvia.lv' + request.fullpath, :status => 301
    end
  end

  def load_common_resources
    @footer_posts = Post.where( "language = ?", all_languages.index(locale) ).limit(5);
  end

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end

  def is_admin?
		return user_signed_in? && current_user.role == "admin"
	end

  def all_languages
    [:lv, :en, :ru]
  end

  private

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
    Rails.application.routes.default_url_options[:locale]= I18n.locale
    session[:locale] = I18n.locale
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << [:provider, :uid]
  end

  def after_sign_in_path_for(resource)
    request.env['omniauth.origin'] || stored_location_for(resource) || root_path
  end

end
