class AboutController < ApplicationController

  before_action :authenticate_user!, :is_admin?, :except => [:index, :show]

  def index
    @page = Page.where( "name = ?", :about_us ).first
  end

  def edit
    @page = Page.where( "name = ?", :about_us ).first
  end

  def update
    @page = Page.where( "name = ?", :about_us ).first

    @page.assign_attributes( clean_params )
    if @page.save
      redirect_to about_path, :notice => "Page was successfully updated."
    else
      render :edit
    end

  end

  private

  def clean_params
    params.require(:page).permit(:id, :content_lv, :content_ru, :content_en)
  end

end
