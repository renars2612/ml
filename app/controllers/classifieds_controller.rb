class ClassifiedsController < ApplicationController

  before_action :authenticate_user!, :except => [:index, :show ]

  impressionist :actions=>[:show]

  def index
    @classifieds = Classified.paginate(:page => params[:page])

    if !params[:sort].blank? && params[:sort] == 'view_count'
      @classifieds = @classifieds.select( "classifieds.*, ( SELECT count(*) FROM impressions WHERE classifieds.id = impressions.impressionable_id AND `impressions`.`impressionable_type` = 'Classified' ) view_count" ).reorder( "view_count DESC" )
    end

    unless params[:category].blank?
      @classifieds = @classifieds.where('category = ?', params[:category] )
    end

    unless params[:search].blank?
      search_string = "%#{params[:search]}%"
      @classifieds = @classifieds.where( "title LIKE ? OR content LIKE ?", search_string, search_string )
    end



    unless params[:ajax].blank?
      unless @classifieds.blank?
        render 'classifieds/_classified_list', :layout => false
      else
        not_found
      end
    end
  end

  def own
    @classifieds = current_user.classifieds.paginate(:page => params[:page])
    unless params[:category].blank?
      @classifieds = @classifieds.where('category = ?', params[:category] )
    end

    unless params[:search].blank?
      search_string = "%#{params[:search]}%"
      @classifieds = @classifieds.where( "title LIKE ? OR content LIKE ?", search_string, search_string )
    end

    unless params[:ajax].blank?
      unless @classifieds.blank?
        render 'classifieds/_classified_list', :layout => false
      else
        not_found
      end
    else
      render :index
    end



  end

  def show
    require 'oembed'

    @classified = Classified.find_by_id!( params[:id] )

  end

  def edit
    @classified = Classified.find_by_id!( params[:id] )

    can_edit?

  end

  def new
    @classified = Classified.new
  end

  def create
    @classified = Classified.new( clean_params )

    @classified.user_id = current_user.id

    if @classified.save
      redirect_to classified_path( :id => @classified.id ), :notice => "Classified was successfully created."
    else
      render :new
    end
  end

  def update
    @classified = Classified.find_by_id!( params[:id] )

    can_edit?

    @classified.assign_attributes( clean_params )
    if @classified.save
      redirect_to classified_path( :id => @classified.id ), :notice => "Classified was successfully created."
    else
      render :edit
    end
  end

  def destroy
    @classified = Classified.find_by_id!( params[:id] )

    can_delete?

    @classified.destroy
    redirect_to classifieds_path, :notice => "Classified was successfully destroyed."
  end

  private

  def clean_params
    params.require(:classified).permit(:id, :title, :category, :content, :user_id, :author_name, :phone, :email, :webpage, :location, :youtube, :soundcloud, :classified_images_attributes =>
      [
        :id, :image, :classified_id
      ])
  end

  def is_author?
		return user_signed_in? && @classified.user_id.to_i == current_user.id
	end

  def can_edit?
    min_since = ( Time.now - @classified.created_at.to_time ) / 60
		unless is_admin? || ( is_author? && min_since < 15 )
      not_found
    end
	end

  def can_delete?
		unless is_admin? || is_author?
      not_found
    end
	end


end
