class LabelsController < ApplicationController

  before_action :authenticate_user!, :is_admin?, :except => [:index, :show]

  def index
    @items = Label.all

    unless params[:search].blank?
      search_string = "%#{params[:search]}%"
      @items = Label.where( "name LIKE ? OR bio_lv LIKE ? OR bio_en LIKE ? OR bio_ru LIKE ?", search_string, search_string, search_string, search_string )
    end

    unless params[:ajax].blank?
      unless @items.blank?
        render "_label_list", :layout => false
      else
        render "_list_not_found", :layout => false
      end
    end
  end

  def show
    @item = Label.find_by_id!( params[:id] )
  end

  def edit
    @item = Label.find_by_id!( params[:id] )
  end

  def new
    @item = Label.new
  end

  def create
    @item = Label.new( clean_params )
    if @item.save
      redirect_to label_path( :id => @item.id ), :notice => "Label was successfully created."
    else
      render :new
    end
  end

  def update
    @item = Label.find_by_id!( params[:id] )
    @item.assign_attributes( clean_params )
    if @item.save
      redirect_to label_path( :id => @item.id ), :notice => "Label was successfully created."
    else
      render :edit
    end
  end

  def destroy
    @item = Label.find_by_id!( params[:id] )

    @item.destroy
    redirect_to labels_path, :notice => "Label was successfully destroyed."
  end

  private

  def clean_params
    params.require(:label).permit(:id, :name, :bio_lv, :bio_en, :bio_ru, :facebook, :twitter, :draugiem, :spotify, :email, :webpage, :contact, :genre_id, :image, :phone)
  end

end
