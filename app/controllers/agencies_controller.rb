class AgenciesController < ApplicationController

  before_action :authenticate_user!, :is_admin?, :except => [:index, :show]

  def index
    @items = Agency.all

    unless params[:search].blank?
      search_string = "%#{params[:search]}%"
      @items = @items.where( "name LIKE ? OR bio_lv LIKE ? OR bio_en LIKE ? OR bio_ru LIKE ?", search_string, search_string, search_string, search_string )
    end

    unless params[:ajax].blank?
      unless @items.blank?
        render "_agency_list", :layout => false
      else
        render "_list_not_found", :layout => false
      end
    end
  end

  def show
    @item = Agency.find_by_id!( params[:id] )
  end

  def edit
    @item = Agency.find_by_id!( params[:id] )
  end

  def new
    @item = Agency.new
  end

  def create
    @item = Agency.new( clean_params )
    if @item.save
      redirect_to agency_path( :id => @item.id ), :notice => "Agency was successfully created."
    else
      render :new
    end
  end

  def update
    @item = Agency.find_by_id!( params[:id] )
    @item.assign_attributes( clean_params )
    if @item.save
      redirect_to agency_path( :id => @item.id ), :notice => "Agency was successfully created."
    else
      render :edit
    end
  end

  def destroy
    @item = Agency.find_by_id!( params[:id] )

    @item.destroy
    redirect_to agencies_path, :notice => "Agency was successfully destroyed."
  end

  private

  def clean_params
    params.require(:agency).permit(:id, :name, :bio_lv, :bio_en, :bio_ru, :facebook, :twitter, :draugiem, :spotify, :email, :webpage, :contact, :genre_id, :image, :phone)
  end

end
