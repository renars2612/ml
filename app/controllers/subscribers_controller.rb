class SubscribersController < ApplicationController

  before_action :authenticate_user!, :is_admin?, :except => [:create]

  def index
    @subscribers = Subscriber.all
  end

  def create
    @subscriber = Subscriber.new( subscriber_params )

    respond_to do |format|

      format.html do
        if @subscriber.save
          redirect_to root_path, :notice => "Subscriber was successfully created."
        else
          redirect_to root_path, :alert => "Subscriber was not created."
        end
      end

      format.json do
        saved = @subscriber.save
        render :json => { :success => saved, :errors => @subscriber.errors }
      end

    end

  end

  def destroy
    @subscriber = Subscriber.find_by_id!( params[:id] )

    @subscriber.destroy
    redirect_to subscribers_path, :notice => "Subscriber was successfully deleted."
  end

  private

  def subscriber_params
    params.require(:subscriber).permit( :email )
  end

end
