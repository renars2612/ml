module ClassifiedsHelper

  def oembed_yt url
    OEmbed::Providers::Youtube.get( url )
  end

  def oembed_sc url
    OEmbed::Providers::SoundCloud.get( url )
  end

  def is_author?
		return user_signed_in? && @classified.user_id.to_i == current_user.id
	end

  def can_edit?
    min_since = ( Time.now - @classified.created_at.to_time ) / 60
		return is_admin? || ( is_author? && min_since < 15 )
	end

  def can_delete?
		return is_admin? || is_author?
	end

end
