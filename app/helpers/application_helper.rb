module ApplicationHelper
  def is_admin?
		return user_signed_in? && current_user.role == "admin"
	end

  def artist_like
    return ['labels','studios','agencies', 'authors', 'venues', 'media', 'buy', 'festivals']
  end

end
