class Post < ActiveRecord::Base

  has_attached_file :image, :styles => { :full => "720x400#" }
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  belongs_to :category

  enum language: [:lv, :en, :ru]

  default_scope { order("publish_date DESC") }

  self.per_page = 10

  def next
    Post.where("publish_date < ?", publish_date).first
  end

  def prev
    Post.where("publish_date > ?", publish_date).last
  end

end
