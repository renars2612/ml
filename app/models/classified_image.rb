class ClassifiedImage < ActiveRecord::Base

  has_attached_file :image, :styles => { :thumb_2 => "200x150#", :thumb => "160x160#" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  belongs_to :classified

end
