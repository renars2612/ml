class Classified < ActiveRecord::Base

  is_impressionable

  belongs_to :user
  has_many :classified_images

  default_scope { order("created_at DESC") }

  accepts_nested_attributes_for :classified_images, allow_destroy: true

  self.per_page = 12

end
