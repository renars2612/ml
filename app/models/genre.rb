class Genre < ActiveRecord::Base

  has_and_belongs_to_many :artists

  default_scope { order("name ASC") }

end
