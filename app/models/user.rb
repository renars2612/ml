class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :rememberable, :omniauthable, :omniauth_providers => [:facebook, :twitter]

  has_many :classifieds

  def self.find_for_oauth(auth)
    where( provider: auth.provider, uid: auth.uid ).first_or_initialize.tap do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.full_name = auth.info.name
      user.image_link = auth.info.image
      user.email = auth.uid+'@'+auth.provider+'.musiclatvia'
      user.password = Devise.friendly_token[0,20]
      user.save!
    end
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

end
