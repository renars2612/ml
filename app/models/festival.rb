class Festival < ActiveRecord::Base

  default_scope { order("start_date ASC, end_date ASC") }

  has_attached_file :image, :styles => { :full => "500x350#", :thumb => "225x145#", :retina_full => "1000x700#", :retina_thumb => "450x290#" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  
end
