// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require_tree ./controllers
//= require lightbox

window.Mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
window.Chrome = /Chrome/i.test(navigator.userAgent);

function share_facebook(btn){


  winHeight = 300;
  winWidth = 600;
  var winTop = (screen.height / 2) - (winHeight / 2);
  var winLeft = (screen.width / 2) - (winWidth / 2);

  var url = window.location.href;

  window.open('http://www.facebook.com/sharer.php?u=' + encodeURIComponent(url) , 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);

}

//--------------------------------------------------------------------------------
function share_twitter(btn){

  winHeight = 300;
  winWidth = 600;
  var winTop = (screen.height / 2) - (winHeight / 2);
  var winLeft = (screen.width / 2) - (winWidth / 2);

  var url = window.location.href;
  var text = $(btn).attr("data-text");

  window.open('http://twitter.com/share?text=' + encodeURIComponent(text) + '&url=' + encodeURIComponent(url) , 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);

}

//--------------------------------------------------------------------------------
function share_draugiem(btn){

  winHeight = 300;
  winWidth = 600;
  var winTop = (screen.height / 2) - (winHeight / 2);
  var winLeft = (screen.width / 2) - (winWidth / 2);

  var url = window.location.href;
  var title = $(btn).attr("data-title");
  var titlePrefix = '';

  var url_dr =  'http://www.draugiem.lv/say/ext/add.php?title=' + encodeURIComponent( title ) +
                '&link=' + encodeURIComponent( url ) +
                ( titlePrefix ? '&titlePrefix=' + encodeURIComponent( titlePrefix ) : '' )

  window.open(url_dr , 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);

}

jQuery(function()
{
  var body = jQuery('body');
  var navigation = jQuery('.nav_wrap');
  var search = jQuery('.search_bar');
  var container = jQuery('.container');

  var controller = container.attr('class').split(' ').filter(function(item)
  {
    return item.split('-')[0] == 'controller';
  })[0].split('-')[1];

  // anchor scroller override

  container.on('click','a',function()
  {
    var target =  jQuery(this);
    var current_path = window.location.href.split("#")[0];
    var href = target.attr('href');


    if( href && href.search('#') != -1 )
    {
      var anchor = href.split('#')[1];
      var target_path = href.split('#')[0];

      if( target_path.length == 0 )
      {
        var scroll_target = container.find('#'+anchor);

  			if( scroll_target.length > 0 )
  			{
  				var pos_top = scroll_target.offset().top - (controller == 'about' ? 90 : 0) ;

  	      jQuery('body,html').animate({ scrollTop: pos_top },300,function()
          {
            history.pushState({}, '', current_path + "#" + anchor);
          });

  	      return false;
  			}
      }


    }
  });

  // sticky side

  if(!Mobile && Chrome)
  {
    var side_navigation = container.find(".side_navigation");


    if( side_navigation.length > 0)
    {
      var side_nav_height = side_navigation.outerHeight(false);
      var side_nav_top = side_navigation.offset().top;
      var container_height =

      side_navigation.on("get_top",function()
      {

        // var scroll_top = jQuery(window).scrollTop();

        side_navigation.css("top", Math.min( Math.max( 0, scroll_top - side_nav_top), container.height() - side_nav_height - 160 ) );

      }).trigger("get_top");

      jQuery(window).on('scroll',function()
      {
        side_navigation.trigger("get_top");
      });
    }
  }

	// mobile navigation
  if(Mobile)
  {
    jQuery("body").addClass("mobile");
    var submenu = navigation.find( 'nav>ul>li>ul, .language_select>ul>li>ul' );

    submenu.siblings('a').click(function()
    {
      var target = jQuery(this);

      target.parent().toggleClass('show_submenu');
      return false;

    });

    container.click(function()
    {
      navigation.find('.show_submenu').removeClass('show_submenu');
    });



    if(jQuery(window).width() < 1025 )
    {
      navigation.on('click', '.nav_button',function()
      {
        navigation.find('nav').toggleClass('active');
        navigation.find('.nav_button').toggleClass('active');
      });
    }
  }

	// search handlers

	body.find('.container > h1').after('<div class="search_error_text">'+ search.find('.not_found_text').text() +'</div>');

	navigation.on('click', '.search', function()
	{
		body.toggleClass('search_active');
	});

	container.on('click', function()
	{
		body.toggleClass('search_active', false );
	});

	search.on('submit','form', function()
	{
		return false
	});

	var search_request;
	search.on('keyup', '.search_input', function(event)
	{
		var key = event.keyCode || event.charCode;
		var target = jQuery(this);
		var form = target.parents('form');

		if( target.val().trim().length > 1 || (key == 8 && target.val().trim().length == 0) )
		{
			if( search_request )
			{
				search_request.abort();
			}
			search_request = jQuery.ajax(
			{
				data: target.val().trim().length > 1 ? form.serialize() : null,
				url: "?ajax=1",
				success: function( data )
				{
					body.removeClass('search_error');
					container.find('.content_wrap').html( data );
				},
				error:function (xhr, ajaxOptions, thrownError)
				{
					if( xhr.status == 404 )
					{
						body.addClass('search_error');
					}
				}
			});
		}

	});

	var user_prefs = ['data-view','data-sort','data-filter'];
	var artist_container = container.find('.artists, .items, .classifieds_wrap');

	if(container.is('.controller-artists, .controller-artist_like, .controller-classifieds'))
	{
		if(container.is('.action-index, .action-own'))
		{

			// load prefs form cookies if there
      user_prefs.forEach(function(pref)
      {
        var saved_pref = get_prefs( controller + "_" + pref);
        if( saved_pref && saved_pref.trim() )
        {
          artist_container.attr(pref,saved_pref);
        }
      });

			// controll status update
			artist_container.on('change', function()
			{
				var target = jQuery(this);
				target.find('.view_controls .active').removeClass('active');

				// save prefs it in cookie
        user_prefs.forEach(function(pref)
        {
          target.find('.view_controls ['+pref+'="'+target.attr(pref)+'"]').addClass('active');
          save_prefs( controller+"_"+pref, target.attr(pref) );
        });

			}).trigger('change');

		}
	}

	if(container.is('.controller-artists, .controller-artist_like'))
	{
		if(container.is('.action-index, .action-own'))
		{
			// sort handlers
			var get_value = function( item )
			{
			  var jq_item = jQuery(item);
			  return jq_item.find('h4').text();
			};

			artist_container.on('sort', function()
			{
				var target = jQuery(this);

				var sort_type = artist_container.attr('data-sort');

				var sorted_items;
				var data_order = "asc";
				var item_wrappers = artist_container.find('.artist_wrap, .item_wrap');

				item_wrappers.each(function()
				{
					item_wrapper = jQuery(this);

					if( sort_type != 'random' )
					{
						sorted_items = item_wrapper.children().sort(function(a,b)
			      {
			        a = get_value( a );
			        b = get_value( b );
			        if( data_order == "asc")
							{
								if(a < b) return -1;
				        if(a > b) return 1;
							}
							else
							{
								if(a < b) return 1;
				        if(a > b) return -1;
							}

			        return  0;
			      });

					}
					else //shuffle
					{
						sorted_items = shuffle( item_wrapper.children() );
					}

					item_wrapper.html( sorted_items );
				});
			});

			// handle controll buttons
			artist_container.on('click', '.view_controls a', function()
			{
				var target = jQuery(this);

				if(target.is('[data-view]'))
				{
					artist_container.attr('data-view', target.attr('data-view') );
          artist_container.trigger('change');
				}
				else if( target.is('[data-sort]') )
				{
					artist_container.attr('data-sort', target.attr('data-sort') );
					artist_container.trigger('sort');
					artist_container.trigger('change');

				}
				else if( target.is('[data-filter]') && artist_container.attr('data-filter') != target.attr('data-filter') )
				{
					artist_container.attr('data-filter', target.attr('data-filter') );
					artist_container.trigger('filter');
				}

				return false;
			});
		}
	}

});
