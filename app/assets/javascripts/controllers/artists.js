jQuery(function()
{
	var container = jQuery('.container');

	if(container.is('.controller-artists'))
	{
		if(container.is('.action-index'))
		{
			var genres = container.find('.genre_wrap ul li, .genre_wrap .view_all');
			var checkboxes = container.find('input[type="checkbox"]');

			container.find('.genres form')[0].reset();

			genres.click(function()
			{
				var target = jQuery(this);
				var checkbox = target.find('input[type="checkbox"]');

				checkbox.prop('checked', !checkbox.prop('checked') ).trigger('change');
				if(!target.is('.view_all'))
				{
					container.find('.genre_wrap .view_all input[type="checkbox"]').prop('checked', false).trigger('change');
				}

				var genres = [];
				container.find('.genres input[type="checkbox"]:checked').each(function(){ genres.push( jQuery(this).attr('name').substr(6).slice(0,-1) ) });
				container.find('.genre').show();

				if(genres.length == 0)
				{
					container.find('.genre_wrap .view_all input[type="checkbox"]').prop('checked', true).trigger('change');
					genres.push('all');
				}

				if( genres.indexOf("all") == -1 )
				{
					container.find('.genre').not( genres.map(function(item){return '[data-genre_id="'+item+'"]'}).join(',') ).hide();
				}


			});

			checkboxes.on('change',function()
			{
				var target = jQuery(this);
				target.parent().toggleClass('active', target.prop('checked'));

			}).trigger('change');
		}
	}

});
