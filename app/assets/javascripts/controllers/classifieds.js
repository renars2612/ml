jQuery(function()
{
	var body = jQuery('body');
	var container = jQuery('.container');

	if(container.is('.controller-classifieds'))
	{

    if( container.is('.action-index') )
		{
			var artist_container = container.find('.classifieds_wrap');

			// handle controll buttons
			artist_container.on('click', '.view_controls a', function()
			{
				var target = jQuery(this);

				if(target.is('[data-view]'))
				{
					artist_container.attr('data-view', target.attr('data-view') );
          artist_container.trigger('change');
				}
				else if( target.is('[data-sort]') )
				{
					artist_container.attr('data-sort', target.attr('data-sort') );
					artist_container.trigger('filter');
					artist_container.trigger('change');

				}
				else if( target.is('[data-filter]') && artist_container.attr('data-filter') != target.attr('data-filter') )
				{
					artist_container.attr('data-filter', target.attr('data-filter') );
					artist_container.trigger('filter');
				}

				return false;
			});


			// filter for classifieds
      artist_container.on('filter',function()
      {
        var basepath = window.location.href.split('?')[0];
        var params = ['ajax=1']

        if( artist_container.attr('data-filter') != '0' )
        {
          params.push('category='+artist_container.attr('data-filter'));
        }

				if( artist_container.attr('data-sort') != '0' )
        {
          params.push('sort='+artist_container.attr('data-sort'));
        }

        jQuery.ajax(
  			{
  				url:      basepath + '?' + params.join('&'),
  				success: function( data )
  				{
						body.removeClass('search_error');
  					artist_container.find('.classifieds').html(data);
            artist_container.trigger('change');
  				},
					error:function (xhr, ajaxOptions, thrownError)
					{
						if( xhr.status == 404 )
						{
							body.addClass('search_error');
						}
					}
  			});
      });
		}

    if(container.is('.action-new') || container.is('.action-edit'))
  	{
      container.on('click','.delete_image',function()
			{
				var target = jQuery(this);

				target.parents('.image_wrap').remove();
				container.find(".upload_button").removeAttr("disabled");
			});

      container.on('click','.upload_button',function()
      {
				var images = container.find('.image_wrap .image_item');

				if( images.length < 4)
				{
					var input_fields = container.find('.image_wrap .image_item input[type="file"]');
					var last_image_id = images.length > 0 && input_fields.length > 0 ? input_fields.last().attr('id').split("_").filter( function(i){ return !isNaN(i) } )[0]*1 + 1 : 0;
					var file_template = container.find('.file_template').html().replace(/_template_/g, last_image_id );
	        container.find('.image_wrap').append(file_template);
	        container.find('.image_wrap .image_item:last-child input[type="file"]').trigger('click');
				}

      });

      container.on('change','input[type="file"]',function()
      {
        var target = jQuery( this );

        var files = target[0].files;
    		// show thumbnail
        for(var i = 0; i<files.length; i++ )
        {
          var reader = new FileReader();
    			reader.onload = function( event )
    			{
            var image = jQuery('<img>');
    				var raw_image = new Image();
    				raw_image.onload = function()
    				{
    					image.attr( 'src', crop( raw_image, 160, 160 ).toDataURL( 'image/jpeg', 90 ) );
              target.parent().append(image);
    				}
    				raw_image.src = event.target.result;

    			}
    			reader.readAsDataURL( files[i] );
        }

				var image_count = container.find('.image_wrap .image_item').length;
				if(image_count == 4)
				{
					container.find(".upload_button").attr("disabled",1);
				}

      });
    }
  }
});
