function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex ;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

var crop = function( src, width, height )
{
	var crop = width == 0 || height == 0;
	// not resize
	if(src.width == width && height == 0){
		height = src.height * (width / src.width);
	}
	// check scale
	var xscale = width  / src.width;
	var yscale = height / src.height;
	var scale  = crop ? Math.min(xscale, yscale) : Math.max(xscale, yscale);
	// create empty canvas
	var canvas = document.createElement("canvas");
	canvas.width  = width ? width   : Math.round(src.width  * scale);
	canvas.height = height ? height : Math.round(src.height * scale);
	canvas.getContext("2d").scale(scale,scale);
	// crop it top center
	canvas.getContext("2d").drawImage(src, ((src.width * scale) - canvas.width) * -.5 , ((src.height * scale) - canvas.height) * .5 );
	return canvas;
}

var setCookie = function(cname, cvalue, exdays)
{
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + cvalue + "; " + expires;
}
var getCookie = function(cname)
{
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i=0; i<ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0)==' ') c = c.substring(1);
      if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
  }
  return "";
}

var save_prefs = function(controller, value)
{
  setCookie(controller, value, 10);
};

var get_prefs = function(controller)
{
  var cookie = getCookie(controller);
  return cookie != "" ? cookie : null;
};
