jQuery(function()
{
  jQuery('body').on('submit', 'form.new_subscriber', function()
  {
    var form = jQuery(this);

    jQuery.ajax(
    {
      type: "post",
      data: form.serialize(),
      url: form.attr('action')+".json",
      success: function( data )
      {
        if(data.success)
        {
          form.parent().addClass('success');
          setTimeout(function(){ form.parent().removeClass('success'); }, 5000);
        }
        else
        {
          form.parent().addClass('duplicate');
          setTimeout(function(){ form.parent().removeClass('duplicate'); }, 5000);
        }
      },
      error:function (xhr, ajaxOptions, thrownError)
      {
        if( xhr.status == 404 )
        {

        }
      }
    });
    return false;
  });

});
