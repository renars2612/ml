

jQuery(function()
{
  var tiny_init = function(elem)
  {
  	var is_float_column = jQuery('#'+elem).parents('.float_column').length > 0;

  	var config = {
  		selector: "#"+elem,
  		toolbar: [" bold italic | link image | code"],
  		plugins: "link code image",
  		fontsize_formats: "14px",
  		invalid_styles: {
  				'*': 'font height margin margin-left margin-right margin-bottom margin-top width text-indent line-height font-size font-family punctuation-wrap text-justify mso-list tab-stops mso-table-layout-alt mso-padding-alt mso-yfti-irow mso-yfti-firstrow mso-pagination mso-layout-grid-align mso-bidi-font-family mso-line-height-rule mso-ignore text-autospace mso-fareast-font-family'
  		},
  		valid_children : "+body[style],+head[style]",
  		setup : function(ed)
  		{
  				ed.on('init', function()
  				{
  						this.getDoc().body.style.fontSize = '14px';
  				});
  		},
  	};

  	tinyMCE.init( config );

  };

	var container = jQuery('.container');

  container.find('textarea.tinymce:visible').each(function()
	{
		var target = jQuery(this);

		tiny_init( target.attr('id') );

	});
});
