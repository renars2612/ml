music_export
============


##Install instructions

dependencies:

mysql - download from http://dev.mysql.com/downloads/mysql/

brew -
ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"

rvm -

\curl -sSL https://get.rvm.io | bash -s stable --ruby

bundler -

gem install bundler

in project root after cloning:

rvm gemset use music_export --create

bundle install
