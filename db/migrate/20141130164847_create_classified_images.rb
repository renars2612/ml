class CreateClassifiedImages < ActiveRecord::Migration
  def change
    create_table :classified_images do |t|

      t.timestamps
    end

    add_attachment :classified_images, :image
    
  end
end
