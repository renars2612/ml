class MoveAboutTemplateToDb < ActiveRecord::Migration
  def change

    @page = Page.where( "name = ?", :about_us ).first

    @page.content_lv = '<div class="side_navigation">
        <div class="links">
          <ul>
            <li>
              <a href="#about-us">Par mums</a>
            </li>
            <li>
              <a href="#history">Vēsture</a>
            </li>
            <li>
              <a href="#administration">Administrācija un kontakti</a>
            </li>
            <li>
              <a href="#members">Biedri</a>
            </li>
            <li>
              <a href="#board">Valde</a>
            </li>
          </ul>
        </div>
      </div>
      <div class="about">
        <div class="paragraph" id="about-us">
          <h2>Par mums</h2>
          <p>Kopš Latvijas Mūzikas attīstības biedrības/ Latvijas Mūzikas eksports dibināšanas 2012. gada ir izveidota un nostiprināta biedrības struktūra un darbības modelis, nodrošinot aktivitāšu izvērsumu gan Latvijā, gan starptautiski.</p>
          <div class="paragraph-list">
            <strong>Mērķi</strong>
            <ul>
              <li>atbalstīt Latvijas izpildītāju un producentu darbību un veicināt mūzikas industrijas attīstību Latvijā un Latvijā producētas mūzikas eksportu</li>
              <li>veicināt un atbalstīt konkurētspējīgu mūzikas ierakstu radīšanu un palielināt latviešu mūzikas izmantošanas apjomu, izglītojot Latvijas izpildītājus un fonogrammu producentus</li>
              <li>būt par Latvijas mūzikas industrijas oficiālo pārstāvi Eiropas un starptautiskajās industrijas konferencēs un izstādēs</li>
              <li>būt par oficiālo sadarbības partneri Baltijas, Eiropas un pasaules mūzikas industriju sadarbības projektos</li>
              <li>apmācīt Latvijas mūzikas industrijas pārstāvjus eksporta jautājumos un globālos mūzikas industrijas jautājumos</li>
            </ul>
          </div>
        </div>
        <div class="paragraph" id="history">
          <h2>Vēsture</h2>
          <p>Biedrība „Latvijas Izpildītāju un producentu apvienība” (turpmāk – LaIPA) un biedrība „Latvijas Mūzikas informācijas centrs” (turpmāk - LMIC) 2012. gada 12.oktobrī pieņēma lēmumu dibināt biedrību „Latvijas Mūzikas attīstības biedrība/Latvijas Mūzikas eksports” (turpmāk – Lmab/Lme), kas 2012.gada 9. novembrī ir reģistrēta LR Uzņēmumu reģistrā. Šāda iniciatīva ir nākusi pēc vairāku gadu diskusijām gan LaIPA valdē, gan biedru kopsapulcēs, gan LMIC lēmējinstitūcijās un ir apskatīti dažādi Latvijas Mūzikas eksporta darbības modeļi un mehānismi. Iemesls diskusijām par Latvijas Mūzikas eksporta nepieciešamību radās pēc fonogrammu izmantotāju iesniegto atskaišu apstrādes un atlīdzības sadales, kas uzrādīja, ka no visa izmantotās mūzikas apjoma un attiecīgi sadalītās atlīdzības tikai 20%-30% no atlīdzībām paliek Latvijā – pārējie 70-80% nonāk lielo ierakstu kompāniju vai citu valstu tiesību īpašnieku kontos. Vienlaikus, no ārvalstīm ienākošā atlīdzība Latvijas izpildītājiem un fonogrammu producentiem (mūzikas ierakstu kompānijām) ir niecīga – tas ir tiešs atspoguļojums tam, ka Latvijā radītās mūzikas izmantojums ir niecīgs. Vērtējot iespēju palielināt Latvijā radītās mūzikas apjomu, LaIPA secināja, ka tas mūzikas proporcionālais sadalījums, kas pastāv Latvijā, ir nostabilizējies un nav tāpat vienkārši maināms. Iemesli tam ir dažādi, tomēr visi no tiem noved pie nepieciešamības radīt labvēlīgus apstākļus, dot iespēju un atbalsta punktu Latvijas izpildītājiem un fonogrammu producentiem – veicināt Latvijā producēto mūzikas ierakstu kvalitāti, piesaistot profesionālus producentus, izglītot tiesību īpašniekus par veidiem kā pašiem producēt savus ierakstus un kā tos veiksmīgi izplatīt, kā arī par dažādām mārketinga metodēm.</p>
          <p>Mūzikas eksporta organizācijas darbojas praktiski visās Eiropas Savienības valstīs vai nu kā Mūzikas informācijas centru satelītorganizācijas vai arī neatkarīgi. Mūzikas eksporta organizāciju uzdevums ir veicināt valsts mūzikas industrijas attīstību, gādāt par starptautisko mārketingu un veicināt Latvijas mūzikas eksportu.</p>
          <div class="paragraph-list">
            <ul>
              <li>
                <a href="#administration">Administrācija un kontakti</a>
              </li>
              <li>
                <a href="#members">Biedri un valde</a>
              </li>
              <li>
                <a href="#board">Statūti</a>
              </li>
            </ul>
          </div>
        </div>
        <div class="paragraph" id="administration">
          <h2>Administrācija un kontakti</h2>
          <p>Izpilddirektore:</p>
          <p>Agnese Cimuška, e-pasts: <a href="mailto:#">agnese@musiclatvia.com</a></p>
          <br>
          <p>Saskaņā ar Lmab/LMe statūtiem izpilddirektors:</p>
          <div class="paragraph-list">
            <ul>
              <li>organizē un nodrošina Biedrības kopsapulces un valdes lēmumu izpildi</li>
              <li>vada un pārzina Biedrības lietas</li>
              <li>bez īpaša pilnvarojuma pārstāv Biedrības intereses valsts un pašvaldību iestādēs,organizācijās, komercsabiedrībās un nevalstiskās organizācijās, tiesās, šķīrējtiesās, kā arī ārvalstu un starptautiskās organizācijās</li>
              <li>izsniedz pilnvaras, atver bankās norēķinu kontus</li>
              <li>nodrošina Biedrības līdzekļu izmantošanu statūtos paredzētiem mērķiem</li>
              <li>pārvalda Biedrības mantu un rīkojas ar tās līdzekļiem atbilstoši šiem Statūtiem un Biedrības valdes lēmumiem</li>
              <li>vadoties no veicamo darbu apjoma, nosaka darbinieku skaitlisko sastāvu un personālo sastāvu, kā arī darba samaksas kārtību un apmēru</li>
              <li>pieņem un atlaiž Biedrības darbiniekus</li>
              <li>nodrošina grāmatvedības, lietvedības un statistikas uzskaites kārtošanu</li>
              <li>Biedrības kopsapulces apstiprinātā budžeta ietvaros slēdz civiltiesiskus darījumus, kārto izdevumus</li>
              <li>izveido Biedrības iekšējās struktūrvienības un nosaka to darbības principus un kārtību</li>
              <li>izlemj citus ikdienas jautājumus un vada citas lietas, kas nav kopsapulces un valdes kompetencē</li>
            </ul>
          </div>
        </div>
        <div class="paragraph" id="members">
          <h2>Biedri</h2>
          <p>Par Biedrības biedru var kļūt juridiska persona, kas vēlas piedalīties Biedrības mērķu sasniegšanā ar aktīvu darbību vai tās atbalstīšanu, iesniedzot noteiktas formas rakstisku pieteikumu.</p>
          <br>
          <p>Ja Lmab/Lme Valde ir uzņēmusi iesniedzēju par biedru, tam jāsamaksā gadamaksa. Juridiskām personām biedra gada maksa 100 EUR apmērā, veicot pārskaitījumu.</p>
          <br>
          <p>Biedrības augstākā lēmējinstitūcija ir biedru kopsapulce.</p>
          <div class="paragraph-list">
            <strong>Biedri</strong>
            <ul>
              <li>SIA „Avantis Promo”</li>
              <li>SIA „Izdevniecība MicRec”</li>
              <li>SIA „de Busul Music”</li>
              <li>SIA „Mūzika Baltika”</li>
              <li>SIA „Instrumenti”</li>
              <li>SIA „Prāta Vētras Skaņu Ierakstu kompānija”</li>
              <li>SIA „TSP Music”</li>
              <li>SIA „Pietura 1984”</li>
              <li>Biedrība „Tālu Skan”</li>
              <li>SIA „Lo-Fi”</li>
              <li>"LaIPA"</li>
              <li>"Latvijas Mūzikas informācijas centrs"</li>
              <li>SIA "Remix Group"</li>
            </ul>
            <br>
          </div>
          <strong>Ja vēlies pievienoties LLMab/LMe, aizpildi un paraksti zemāk pievienoto iestāšanās pieteikuma anketu un nosūti to <a href="#">mums.</a></strong>
          <div class="paragraph-list">
            <strong>Kopsapulces kompetence:</strong>
            <ul>
              <li>grozījumu izdarīšana statūtos</li>
              <li>valdes un revīzijas institūciju locekļu ievēlēšana un atsaukšana, ja statūtos šādas tiesības nav piešķirtas citai pārvaldes institūcijai</li>
              <li>lēmuma pieņemšana par biedrības darbības izbeigšanu, turpināšanu vai reorganizāciju</li>
              <li>citi jautājumi, kuri saskaņā ar likumu vai statūtiem ir biedru sapulces kompetencē</li>
              <li>Biedru sapulcei ir tiesības pieņemt arī tādus lēmumus, kas ietilpst valdes un citu statūtos paredzēto institūciju kompetencē, ja statūtos nav noteikts citādi</li>
            </ul>
          </div>
        </div>
        <div class="paragraph" id="board">
          <h2>Valde</h2>
          <p>Biedrības izpildinstitūcija ir valde, kas sastāv no deviņiem valdes locekļiem, no kuriem 5 valdes locekļi ir LaIPA valdes apstiprināti pārstāvji, nodrošinot žanrisko daudzveidību.</p>
          <br>
          <ul class="members">
            <li></li>
            <strong>Valdes locekļi:</strong>
            <li>Liena Grīna</li>
            <li>Ināra Jakubone</li>
            <li>Solvita Sējāne</li>
            <li>Kristīne Kutuzova</li>
            <li>Kārlis Auzāns</li>
            <li>Kārlis Būmeisters</li>
            <li>Arvīds Mūrnieks</li>
            <li>Juris Zalāns</li>
            <li>Artis Sīmanis</li>
          </ul>
          <br>
          <div class="paragraph-list">
            <strong>Valdes funkcijas:</strong>
            <ul>
              <li>Valde ir tiesīga izlemt visus jautājumus, kas nav ekskluzīvā biedru sapulces kompetencē</li>
              <li>Valde pārstāv biedru interesēs laikā starp kopsapulcēm;</li>
              <li>Valdes locekļi ir tiesīgi pārstāvēt Biedrību atsevišķi un/vai pilnvarot trešo personu pārstāvēt Biedrību</li>
            </ul>
          </div>
          <br>
          <p>Valdes locekļus nominē un ieceļ ar Biedru kopsapulces lēmumu uz 2 gadiem.Biedrības pirmās valdes darbības termiņš beidzas ne ātrāk kā otrajā biedru kārtējā kopsapulcē pēc Biedrības dibināšanas.</p>
        </div>
      </div>'

      @page.save

  end
end
