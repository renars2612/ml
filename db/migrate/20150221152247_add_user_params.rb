class AddUserParams < ActiveRecord::Migration
  def change
    add_column :users, :role, :string
    add_column :users, :full_name, :string
  end
end
