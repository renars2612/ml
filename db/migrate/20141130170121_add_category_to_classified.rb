class AddCategoryToClassified < ActiveRecord::Migration
  def change
    add_column :classifieds, :category, :integer
  end
end
