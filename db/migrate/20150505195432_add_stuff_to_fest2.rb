class AddStuffToFest2 < ActiveRecord::Migration
  def change
    add_column :festivals, :email, :string
    add_column :festivals, :phone, :string
  end
end
