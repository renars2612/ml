class AddI18nCatNames < ActiveRecord::Migration
  def change
    add_column :categories, :name_lv, :string
    add_column :categories, :name_en, :string
    add_column :categories, :name_ru, :string

    Category.all.each do |cat|
      cat.update_attributes( :name_lv => cat.name )
    end
    remove_column :categories, :name
  end
end
