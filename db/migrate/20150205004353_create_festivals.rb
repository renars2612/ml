class CreateFestivals < ActiveRecord::Migration
  def change
    create_table :festivals do |t|
      t.string :name
      t.date :start_date
      t.date :end_date
      t.timestamps null: false
    end

    add_column :festivals, :location, :string
    add_column :festivals, :twitter, :string
    add_column :festivals, :facebook, :string
    add_column :festivals, :draugiem, :string
    add_column :festivals, :homepage, :string
    add_column :festivals, :lineup_link, :string
    
  end
end
