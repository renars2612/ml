class AddClassifiedId < ActiveRecord::Migration
  def change
    add_column :classified_images, :classified_id, :integer
  end
end
