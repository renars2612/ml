class AddGenres < ActiveRecord::Migration
  def change
    ["rock","pop","experimental","jazz / blues","classical","hip hop / r&b","folk / acoustic","world","metal","electronic","instrumental"].each do |genre|
      Genre.create(:name => genre)
    end
  end
end
