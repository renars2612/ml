class AddSpotlightBool < ActiveRecord::Migration
  def change
    add_column :artists, :spotlight, :boolean
  end
end
