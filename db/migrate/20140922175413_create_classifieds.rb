class CreateClassifieds < ActiveRecord::Migration
  def change
    create_table :classifieds do |t|
      t.string :title
      t.text :content_lv
      t.text :content_en
      t.string :user_id
      t.string :author_name
      t.string :phone
      t.string :email
      t.string :webpage
      t.string :location
      t.string :youtube
      t.string :soundcloud
      t.timestamps
    end
  end
end
