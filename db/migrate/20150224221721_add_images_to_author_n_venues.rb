class AddImagesToAuthorNVenues < ActiveRecord::Migration
  def change
    def self.up
      add_attachment :authors, :image
      add_attachment :venues, :image
    end

    def self.down
      remove_attachment :authors, :image
      remove_attachment :venues, :image
    end
  end
end
