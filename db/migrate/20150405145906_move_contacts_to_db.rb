class MoveContactsToDb < ActiveRecord::Migration
  def change

    @page = Page.new

    content = '<h4 class="underline">Agnese Cimuška</h4>
    <p>agnese@musiclatvia.lv</p>
    <p>+371 26457262</p>'

    @page.name = :contacts
    @page.content_lv = content
    @page.content_ru = content
    @page.content_en = content

    @page.save

  end
end
