class AddImgMedia < ActiveRecord::Migration
  def change
    def self.up
      add_attachment :media, :image
    end

    def self.down
      remove_attachment :media, :image
    end
  end
end
