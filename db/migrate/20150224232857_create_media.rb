class CreateMedia < ActiveRecord::Migration
  def change
    create_table :media do |t|
      t.string :name
      t.text :bio_en
      t.text :bio_lv
      t.text :bio_ru
      t.string :facebook
      t.string :twitter
      t.string :draugiem
      t.string :spotify
      t.string :email
      t.string :webpage
      t.string :contact
      t.text :soundcloud

      t.timestamps
    end
  end
end
