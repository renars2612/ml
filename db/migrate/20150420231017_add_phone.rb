class AddPhone < ActiveRecord::Migration
  def change
    add_column :agencies, :phone, :string
    add_column :authors, :phone, :string
    add_column :labels, :phone, :string
    add_column :products, :phone, :string
    add_column :media, :phone, :string
    add_column :studios, :phone, :string
    add_column :venues, :phone, :string
  end
end
