class RenameContentClassfield < ActiveRecord::Migration
  def change
    rename_column :classifieds, :content_lv, :content
    remove_column :classifieds, :content_en

  end
end
