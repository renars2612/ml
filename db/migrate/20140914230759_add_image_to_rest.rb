class AddImageToRest < ActiveRecord::Migration
  def self.up
    add_attachment :labels, :image
    add_attachment :studios, :image
    add_attachment :agencies, :image
  end

  def self.down
    remove_attachment :labels, :image
    remove_attachment :studios, :image
    remove_attachment :agencies, :image
  end
end
