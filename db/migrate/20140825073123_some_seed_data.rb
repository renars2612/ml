class SomeSeedData < ActiveRecord::Migration
  def change
    lorem = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?'
    10.times do
      Post.create(:title => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', :content => lorem )
    end

    10.times do |index|
      Artist.create(
                    :name => 'Lorem ipsum',
                    :bio_en => lorem, :bio_lv => lorem, :bio_ru => lorem,
                    :genre_id => (index % 10 + 1),
                    :facebook => 'https://www.facebook.com/loremipsum2014',
                    :twitter => 'https://www.facebook.com/loremipsum2014',
                    :draugiem => 'https://www.facebook.com/loremipsum2014',
                    :webpage => 'https://www.facebook.com/loremipsum2014',
                    :spotify => 'https://www.facebook.com/loremipsum2014',
                    :email => 'hello@jveart.com',
                    :contact => 'tel: 1231423143',
                    :soundcloud => '<iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/5517723&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>'
                      )
    end
  end
end
