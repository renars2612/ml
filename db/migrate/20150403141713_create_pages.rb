class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.text :content_lv
      t.text :content_ru
      t.text :content_en
      t.timestamps null: false
    end
  end
end
