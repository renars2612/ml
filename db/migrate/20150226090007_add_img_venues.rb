class AddImgVenues < ActiveRecord::Migration
  def self.up
    add_attachment :authors, :image
    add_attachment :venues, :image
    add_attachment :media, :image
  end

  def self.down
    remove_attachment :authors, :image
    remove_attachment :venues, :image
    remove_attachment :media, :image
  end
end
