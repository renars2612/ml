class AddStuffToFest < ActiveRecord::Migration
  def change
    add_column :festivals, :bio_en, :text
    add_column :festivals, :bio_lv, :text
    add_column :festivals, :bio_ru, :text
    add_column :festivals, :spotify, :string
    add_column :festivals, :contact, :string
    add_column :festivals, :soundcloud, :text
    rename_column :festivals, :homepage, :webpage
    add_attachment :festivals, :image
  end
end
