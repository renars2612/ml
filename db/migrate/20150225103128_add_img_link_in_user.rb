class AddImgLinkInUser < ActiveRecord::Migration
  def change
    add_column :users, :image_link, :string
  end
end
