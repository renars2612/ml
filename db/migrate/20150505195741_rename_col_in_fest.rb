class RenameColInFest < ActiveRecord::Migration
  def change
    remove_column :festivals, :webpage
    rename_column :festivals, :lineup_link, :webpage
  end
end
