class HistoryToDb < ActiveRecord::Migration
  def change
    content = '
        <div class="img_title"></div>
        <div class="content_item">
          <div class="img_thumbnail milda"></div>
          <div class="item_content">
            <h2>Neatkarība</h2>
            <p>Vēsturē mēs esam cīnijušies par savu brīvību ar dziesmu. Par pierādījumu mums tam ir savs Brīvības piemineklis, kurš vienmēr atgādina midzēt kā zvaigznēm.</p>
          </div>
          <div class="clear"></div>
        </div>
        <div class="content_item">
          <div class="img_thumbnail gaismasp"></div>
          <div class="item_content">
            <h2>Zināšanas</h2>
            <p>Mēs zinam, ka jaunu radīt varam no aizmirsta vecā. Mēs neaizmirstam. Latvijas nacionalajā bibliotēkā glabājam partitūras, senus skaņu ierakstus un mūzikas literatūru.</p>
          </div>
          <div class="clear"></div>
        </div>
        <div class="content_item">
          <div class="img_thumbnail elements"></div>
          <div class="item_content">
            <h2>Eksports</h2>
            <p>Mūsu mūzikas kuģus pūš no ziemeļiem. Mēs lidojam gaisā dzīvajā un straumējam mūziku gan internetā gan pāri Baltijas jūrai.</p>
          </div>
          <div class="clear"></div>
        </div>
        <div class="content_item">
          <div class="img_thumbnail dzsv"></div>
          <div class="item_content">
            <h2>Gars</h2>
            <p>Mēs esam dziedātāju tauta. Jau 140 gadus mums pieder pasaulē atzīts mākslas un kultūras brīnums “Vispārējie latviešu Dziesmu un Deju svētki”. Tas ir mūsu gara tilts, kā apzinamies savu vienotību un latviskumu.</p>
          </div>
          <div class="clear"></div>
        </div>'
    Page.create( :name => :history, :content_lv => content, :content_en => content, :content_ru => content )
  end
end
