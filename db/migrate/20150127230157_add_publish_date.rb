class AddPublishDate < ActiveRecord::Migration
  def change
    add_column :posts, :publish_date, :date

    Post.all.each do |post|
      post.publish_date = post.created_at.to_date
      post.save
    end
  end
end
