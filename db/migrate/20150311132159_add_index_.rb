class AddIndex < ActiveRecord::Migration
  def change

    add_index :artists_genres, [:artist_id, :genre_id]
  end
end
