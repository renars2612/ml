class Fix < ActiveRecord::Migration
  def change
    Artist.all.each do |artist|
      ArtistsGenre.create(:artist_id => artist.id, :genre_id => artist.genre_id )
    end

    remove_column :artists, :genre_id
  end
end
