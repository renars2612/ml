# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150505195741) do

  create_table "agencies", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.text     "bio_en",             limit: 65535
    t.text     "bio_lv",             limit: 65535
    t.text     "bio_ru",             limit: 65535
    t.string   "facebook",           limit: 255
    t.string   "twitter",            limit: 255
    t.string   "draugiem",           limit: 255
    t.string   "spotify",            limit: 255
    t.string   "email",              limit: 255
    t.string   "webpage",            limit: 255
    t.string   "contact",            limit: 255
    t.text     "soundcloud",         limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
    t.string   "phone",              limit: 255
  end

  create_table "artists", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.text     "bio_en",             limit: 65535
    t.text     "bio_lv",             limit: 65535
    t.text     "bio_ru",             limit: 65535
    t.string   "facebook",           limit: 255
    t.string   "twitter",            limit: 255
    t.string   "draugiem",           limit: 255
    t.string   "spotify",            limit: 255
    t.string   "email",              limit: 255
    t.string   "webpage",            limit: 255
    t.string   "contact",            limit: 255
    t.text     "soundcloud",         limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
    t.boolean  "spotlight",          limit: 1
    t.string   "phone",              limit: 255
  end

  create_table "artists_genres", id: false, force: :cascade do |t|
    t.integer "artist_id", limit: 4
    t.integer "genre_id",  limit: 4
  end

  add_index "artists_genres", ["artist_id", "genre_id"], name: "index_artist_genres_on_artist_id_and_genre_id", using: :btree

  create_table "authors", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.text     "bio_en",             limit: 65535
    t.text     "bio_lv",             limit: 65535
    t.text     "bio_ru",             limit: 65535
    t.string   "facebook",           limit: 255
    t.string   "twitter",            limit: 255
    t.string   "draugiem",           limit: 255
    t.string   "spotify",            limit: 255
    t.string   "email",              limit: 255
    t.string   "webpage",            limit: 255
    t.string   "contact",            limit: 255
    t.text     "soundcloud",         limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
    t.string   "phone",              limit: 255
  end

  create_table "categories", force: :cascade do |t|
    t.string "name_lv", limit: 255
    t.string "name_en", limit: 255
    t.string "name_ru", limit: 255
  end

  create_table "classified_images", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
    t.integer  "classified_id",      limit: 4
  end

  create_table "classifieds", force: :cascade do |t|
    t.string   "title",       limit: 255
    t.text     "content",     limit: 65535
    t.string   "user_id",     limit: 255
    t.string   "author_name", limit: 255
    t.string   "phone",       limit: 255
    t.string   "email",       limit: 255
    t.string   "webpage",     limit: 255
    t.string   "location",    limit: 255
    t.string   "youtube",     limit: 255
    t.string   "soundcloud",  limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "category",    limit: 4
  end

  create_table "festivals", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "location",           limit: 255
    t.string   "twitter",            limit: 255
    t.string   "facebook",           limit: 255
    t.string   "draugiem",           limit: 255
    t.string   "webpage",            limit: 255
    t.text     "bio_en",             limit: 65535
    t.text     "bio_lv",             limit: 65535
    t.text     "bio_ru",             limit: 65535
    t.string   "spotify",            limit: 255
    t.string   "contact",            limit: 255
    t.text     "soundcloud",         limit: 65535
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
    t.string   "email",              limit: 255
    t.string   "phone",              limit: 255
  end

  create_table "genres", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "impressions", force: :cascade do |t|
    t.string   "impressionable_type", limit: 255
    t.integer  "impressionable_id",   limit: 4
    t.integer  "user_id",             limit: 4
    t.string   "controller_name",     limit: 255
    t.string   "action_name",         limit: 255
    t.string   "view_name",           limit: 255
    t.string   "request_hash",        limit: 255
    t.string   "ip_address",          limit: 255
    t.string   "session_hash",        limit: 255
    t.text     "message",             limit: 65535
    t.text     "referrer",            limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "impressions", ["controller_name", "action_name", "ip_address"], name: "controlleraction_ip_index", using: :btree
  add_index "impressions", ["controller_name", "action_name", "request_hash"], name: "controlleraction_request_index", using: :btree
  add_index "impressions", ["controller_name", "action_name", "session_hash"], name: "controlleraction_session_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "ip_address"], name: "poly_ip_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "request_hash"], name: "poly_request_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "session_hash"], name: "poly_session_index", using: :btree
  add_index "impressions", ["impressionable_type", "message", "impressionable_id"], name: "impressionable_type_message_index", length: {"impressionable_type"=>nil, "message"=>255, "impressionable_id"=>nil}, using: :btree
  add_index "impressions", ["user_id"], name: "index_impressions_on_user_id", using: :btree

  create_table "labels", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.text     "bio_en",             limit: 65535
    t.text     "bio_lv",             limit: 65535
    t.text     "bio_ru",             limit: 65535
    t.string   "facebook",           limit: 255
    t.string   "twitter",            limit: 255
    t.string   "draugiem",           limit: 255
    t.string   "spotify",            limit: 255
    t.string   "email",              limit: 255
    t.string   "webpage",            limit: 255
    t.string   "contact",            limit: 255
    t.text     "soundcloud",         limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
    t.string   "phone",              limit: 255
  end

  create_table "media", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.text     "bio_en",             limit: 65535
    t.text     "bio_lv",             limit: 65535
    t.text     "bio_ru",             limit: 65535
    t.string   "facebook",           limit: 255
    t.string   "twitter",            limit: 255
    t.string   "draugiem",           limit: 255
    t.string   "spotify",            limit: 255
    t.string   "email",              limit: 255
    t.string   "webpage",            limit: 255
    t.string   "contact",            limit: 255
    t.text     "soundcloud",         limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
    t.string   "phone",              limit: 255
  end

  create_table "pages", force: :cascade do |t|
    t.text     "content_lv", limit: 65535
    t.text     "content_ru", limit: 65535
    t.text     "content_en", limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "name",       limit: 255
  end

  create_table "posts", force: :cascade do |t|
    t.string   "title",              limit: 255
    t.text     "content",            limit: 65535
    t.integer  "language",           limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "publish_date"
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
    t.integer  "category_id",        limit: 4
  end

  create_table "products", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.text     "bio_en",             limit: 65535
    t.text     "bio_lv",             limit: 65535
    t.text     "bio_ru",             limit: 65535
    t.string   "facebook",           limit: 255
    t.string   "twitter",            limit: 255
    t.string   "draugiem",           limit: 255
    t.string   "spotify",            limit: 255
    t.string   "email",              limit: 255
    t.string   "webpage",            limit: 255
    t.string   "contact",            limit: 255
    t.text     "soundcloud",         limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
    t.string   "phone",              limit: 255
  end

  create_table "studios", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.text     "bio_en",             limit: 65535
    t.text     "bio_lv",             limit: 65535
    t.text     "bio_ru",             limit: 65535
    t.string   "facebook",           limit: 255
    t.string   "twitter",            limit: 255
    t.string   "draugiem",           limit: 255
    t.string   "spotify",            limit: 255
    t.string   "email",              limit: 255
    t.string   "webpage",            limit: 255
    t.string   "contact",            limit: 255
    t.text     "soundcloud",         limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
    t.string   "phone",              limit: 255
  end

  create_table "subscribers", force: :cascade do |t|
    t.string   "email",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "provider",               limit: 255
    t.string   "uid",                    limit: 255
    t.string   "role",                   limit: 255
    t.string   "full_name",              limit: 255
    t.string   "image_link",             limit: 255
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "venues", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.text     "bio_en",             limit: 65535
    t.text     "bio_lv",             limit: 65535
    t.text     "bio_ru",             limit: 65535
    t.string   "facebook",           limit: 255
    t.string   "twitter",            limit: 255
    t.string   "draugiem",           limit: 255
    t.string   "spotify",            limit: 255
    t.string   "email",              limit: 255
    t.string   "webpage",            limit: 255
    t.string   "contact",            limit: 255
    t.text     "soundcloud",         limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
    t.string   "phone",              limit: 255
  end

end
