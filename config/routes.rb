Rails.application.routes.draw do

  match '/404', to: 'errors#file_not_found', via: :all
  match '/422', to: 'errors#unprocessable', via: :all
  match '/500', to: 'errors#internal_server_error', via: :all

  devise_for :users, skip: [:session, :password, :registration], controllers: { omniauth_callbacks: "users/omniauth_callbacks" }

  scope "(:locale)", :locale => /lv|en|ru/ do
    root 'home#index'

    devise_for :users, skip: [:omniauth_callbacks]

    resources :news

    resources :music, :only => [] do
      collection do
        resources :artists
        resources :labels
        resources :studios
        resources :agencies
        resources :authors
        resources :venues
        resources :media
        resources :buy
        get 'history' => 'history#index'
      end
    end

    resources :live, :only => [] do
      collection do
        resources :festivals
      end
    end

    get 'contacts' => 'contacts#index'
    post 'contacts' => 'contacts#update'
    patch 'contacts' => 'contacts#update'
    put 'contacts' => 'contacts#update'
    get 'contacts/edit' => 'contacts#edit'

    get 'about' => 'about#index'
    post 'about' => 'about#update'
    patch 'about' => 'about#update'
    put 'about' => 'about#update'
    get 'about/edit' => 'about#edit'


    resources :classifieds do
      collection do
        get 'own'
      end
    end

    resources :subscribers, :only => [ :index, :destroy, :create ]
  end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  #

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
