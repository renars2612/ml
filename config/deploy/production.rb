# Define roles, user and IP address of deployment server
# role :name, %{[user]@[IP adde.]}
role :app, %w{deploy@178.62.219.221}
role :web, %w{deploy@178.62.219.221}
role :db,  %w{deploy@178.62.219.221}

# Define server(s)
server '178.62.219.221', user: 'deploy', roles: %w{web}

# SSH Options
# See the example commented out section in the file
# for more options.
set :ssh_options, { :forward_agent => true }

set :rvm_ruby_version, '2.2.3@music_export --create'

set :deploy_to, '~/ruby/music_export/'

set :rails_env, 'production'
