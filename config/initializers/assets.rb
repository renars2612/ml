Rails.application.config.assets.precompile += %w( datepicker.css )
Rails.application.config.assets.precompile += %w( tinymce_config.js )
Rails.application.config.assets.precompile += %w( admin.js )
Rails.application.config.assets.precompile += %w( lightbox/* )
